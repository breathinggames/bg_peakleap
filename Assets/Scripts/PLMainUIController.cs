﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class PLMainUIController : MonoBehaviour, IEventSystemHandler
{

    public static PLMainUIController instance;

    [SerializeField]
    AudioClip beepSound1, beepSound2, blowSound;

    bool setPFSlider;

    [SerializeField]
    Text peakFlowResult, predictedVolume, readySetGoText, starsCollected, messageText;

    public GameObject setPredictedBreathPanel, readySetGoPanel, peakFlowSliderPanel, stageCompletePanel, gameOverPanel;

    [SerializeField]
    Slider peakFlowSlider;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        CallReadySetGo();
        predictedVolume.text = "PREDICTED VOLUME: " + PLPeakFlowTest.instance.predictedVolume.ToString();
    }


    void Update()
    {
        if (PLBallController.instance.peakFlowPerformed == false)
        {
            peakFlowResult.text = PLPeakFlowTest.instance.breathPower.ToString();
        }

        if (setPFSlider)
        {
            SetPeakFlowSlider();
        }
    }

    // a panel in which you can set your 'predicted max volume', to be calculated elswehere based on your age, height, sex, etc.
    // currently disabled, as the breath scale is always set to be the same so it doesn't matter what your predicted volume is in relation
    // to the power you get from a blow.
    public void ChangePredictedBreath(int ammount)
    {
        PLPeakFlowTest.instance.predictedVolume += ammount;
        predictedVolume.text = "PREDICTED VOLUME: " + PLPeakFlowTest.instance.predictedVolume.ToString();
    }

    public void ClosePredictedBreathPanel()
    {
        setPredictedBreathPanel.SetActive(false);
        StartCoroutine("ReadySetGoText");
    }

    void CallReadySetGo()
    {
        StartCoroutine("ReadySetGoText");
    }

    // turns on the text panel, and displays the 'ready, set, blow' text, after which peak flow input is enabled.
    IEnumerator ReadySetGoText()
    {
        yield return new WaitForSeconds(2f);
        stageCompletePanel.SetActive(false);
        readySetGoPanel.SetActive(true);
        readySetGoText.text = "READY?";
        PLGameController.instance.PlaySound(beepSound1);
        yield return new WaitForSeconds(1f);
        readySetGoText.text = "SET...";
        PLGameController.instance.PlaySound(beepSound1);
        yield return new WaitForSeconds(1f);
        readySetGoText.text = "BLOW!!!";
        PLGameController.instance.PlaySound(beepSound2);
        PLBallController.instance.ready = true;
        yield return new WaitForSeconds(1f);
        readySetGoPanel.SetActive(false);
        PLPeakFlowTest.instance.ResetBreathPower();
        peakFlowSliderPanel.SetActive(true);
        setPFSlider = true;
    }    

    // called from other scripts to display how many stars youve collected
    public void DisplayStars(int ammount)
    {
        starsCollected.text = ammount.ToString();
    }

    // called from update when you are doing a peak flow to set the slider value
    void SetPeakFlowSlider()
    {
        peakFlowSlider.value = PLPeakFlowTest.instance.ReturnPeakFlowPercentage();
    }

    public void DisablePeakFlowSlider()
    {
        peakFlowSliderPanel.SetActive(false);
        setPFSlider = false;
    }

    // displays the stage complete text, before calling readysetgo after a short wait
    public void StageCompletePanel()
    {
        stageCompletePanel.SetActive(true);
        Invoke("CallReadySetGo", 3f);
    }

    // turns on the gameover panel, allowing the player to quit
    public void GameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }


    public void Quit()
    {
        Application.Quit();
    }

    public void DisplayMessage(string message)
    {
        messageText.text = message;
        //Invoke("ResetMessage", 3f);
    }

    public void ResetMessage()
    {
        messageText.text = "";
    }


    // an attempt at using the Unity InputField component to be able to type in your Predicted Volume. 
    // didn't manage to get it working so we are temporarily using buttons to increase Predicted Volume in increments

    /*public void SetPredictedBreath(Text input)
    {        
        PLPeakFlowTest.instance.predictedVolume = int.Parse(input.ToString());
        predictedVolume.text = input.ToString();
        setPredictedBreathPanel.SetActive(false);
        PLBallController.instance.predictedVolumeSet = true;
    }*/

}
