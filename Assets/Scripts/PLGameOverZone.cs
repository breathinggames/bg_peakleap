﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLGameOverZone : MonoBehaviour
{

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // when the player enters this trigger zone, plays the fireworks animation and sets the game as completed
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            PLGameController.instance.GameComplete();
            anim.enabled = true;
        }
    }



}
