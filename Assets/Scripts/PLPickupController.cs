﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLPickupController : MonoBehaviour
{

    // how many stars you get from this pickup. can be increased for higher value stars
    [SerializeField]
    int starValue = 1;

    // particles that explode out when you pickup a star
    public GameObject starCollectPrefab;

    // when called by the player, instantiate the pickup particle effects, increase the stars the player has, then destroys the object
    public int CollectStars()
    {
        Instantiate(starCollectPrefab, transform.position, PLGameController.instance.gameObject.transform.rotation);
        PLGameController.instance.IncreaseStars(starValue);
        Invoke("DestroyStar", 0.01f);
        return starValue;
    }

    void DestroyStar()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

}
