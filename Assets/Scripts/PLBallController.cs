﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLBallController : MonoBehaviour
{

    // OVERALL LOGIC: when the ball is created, the player holds down the mouse button to 'charge' the peak flow (a temporary solution until we have spirometer input)
    // after releasing, the ball is fired forwards with a force dependent on the peak flow performed, but also regulated so it can't go too slow or too fast to mess with the gameplay
    // when the ball lands on a platform its forward velocity is then regulated, again to ensure it can't slow down or speed up too much.
    // when it hits an 'endzone' the ball is reset, ready for the next stage

    public static PLBallController instance;

    // components
    Rigidbody rb;
    AudioSource source;

    [SerializeField]
    AudioClip pickupStar, peakLeap;

    // the force thats applied for sideways movement
    [SerializeField]
    float lateralMovementForce = 10;
    [SerializeField]
    float mobileLateralMovementForce = 50;

    // the minimum velocity the ball will move when on a platform
    [SerializeField]
    float minimumForwardVelocity = 20;

    // the min and max allowable forces the ball can launch at, regardless of the peakflow    
    [SerializeField]
    float maxLaunchForce = 10000;
    [SerializeField]
    float minLaunchForce = 5000;

    // bools
    bool moveForwards;
    public bool peakFlowPerformed;
    public bool ready;
    public bool horizontalMovementAllowed;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!ready) { return; }
        InputControls();
#if UNITY_ANDROID
        AndroidInput();
#endif

    }

    void FixedUpdate()
    {
        if (moveForwards)
        {
            ConstantForwardsMovement();
        }
    }

    void OnCollisionStay(Collision coll)
    {
        // when the ball touches a platform, ensure that it moves forward at a constant speed.
        if (coll.gameObject.tag == "FloorTile" && peakFlowPerformed)
        {
            HorizontalMovementAllowed(true);
            RegulateForwardMovement(true);
        }
    }


    void OnTriggerEnter(Collider coll)
    {
        // for collecting stars
        if (coll.gameObject.tag == "Star")
        {
            source.PlayOneShot(pickupStar);
            coll.gameObject.GetComponent<PLPickupController>().CollectStars();
        }
    }

    // controls for performing peak flows and moving horizontally
    void InputControls()
    {
        // while the mouse is pressed, charge the peak flow
        if (Input.GetButtonDown("Fire1") && !peakFlowPerformed)
        {
            PLPeakFlowTest.instance.PerformPeakFlow();
        }
        // when it is released, launch
        if (Input.GetButtonUp("Fire1") && !peakFlowPerformed)
        {
            LaunchBall();
        }

        if(!horizontalMovementAllowed) { return; }
        if (Input.GetKey(KeyCode.D))
        {
            MoveRight();
        }
        if (Input.GetKey(KeyCode.A))
        {
            MoveLeft();
        }
    }


    void LaunchBall()
    {        
        PLPeakFlowTest.instance.FinishPeakFlow();
        PeakFlowCompleted(true);
        rb.AddForce(Vector3.forward * RegulatedLaunchForce());
        PLMainUIController.instance.DisablePeakFlowSlider();
        source.PlayOneShot(peakLeap);        
    }

    void PeakFlowCompleted(bool active)
    {
        peakFlowPerformed = active;
    }

    void HorizontalMovementAllowed(bool active)
    {
        horizontalMovementAllowed = active;
    }

    // launch the ball according to the float returned by PLPeakFlowTest. 
    // also ensure that the ball never launches too slowly (to fall off the edge), or too fast (and go into outer space!) regardless of the peak flow result.    
    float RegulatedLaunchForce()
    {
        float launchForce = 8000f;            
        launchForce += PLPeakFlowTest.instance.ReturnPeakFlowPercentage() * 50;

        Debug.Log("Launch Force was: " + launchForce);
        if (launchForce > maxLaunchForce)
        {
            launchForce = maxLaunchForce;
        }
        if (launchForce < minLaunchForce)
        {
            launchForce = minLaunchForce;
        }
        Debug.Log("Launch Force changed to: " + launchForce);
        return launchForce;
    }


    // when moveForwards is turned on, ConstantForwardsMovement is applied
    void RegulateForwardMovement(bool active)
    {
        moveForwards = active;
    }

    // this is to stop it slowing down or speeding up too much
    void ConstantForwardsMovement()
    {
        if (rb.velocity.magnitude < minimumForwardVelocity)
        {
            Vector3 forwardForce = new Vector3(rb.velocity.x, rb.velocity.y, minimumForwardVelocity);
            rb.velocity = forwardForce;
        }
    }

    // horizontal tilt control for android
    void AndroidInput()
    {
        if (!horizontalMovementAllowed) { return; }
        Vector3 latMov = Input.acceleration * mobileLateralMovementForce;
        rb.AddForce(latMov);
    }


    // adds lateral force to move the player left or right
    void MoveRight()
    {
        Vector3 latMov = (Vector3.right * lateralMovementForce);
        rb.AddForce(latMov);
    }
    void MoveLeft()
    {
        Vector3 latMov = (Vector3.left * lateralMovementForce);
        rb.AddForce(latMov);
    }


    // resets all the bools needed to get the player ready for a new peak flow launch. also resets his velocity in case he is drifting
    public void ResetForNextStage()
    {
        PeakFlowCompleted(false);
        RegulateForwardMovement(false);        
        HorizontalMovementAllowed(false);
        ready = false;        
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        rb.isKinematic = false;
    }

    // disables the player from being able to move at the end of the game, so he doesnt keep running into the wall
    public void Disable()
    {
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
    }

}
