﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLEndZoneController : MonoBehaviour
{

    [SerializeField]
    Transform resetPoint;


    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // if the player hits this trigger zone he has completed a stage. anim is turned on to show fireworks, and the players
    // position is set to the resetPoint transform, so that he is always correctly lined up for the next peak flow launch
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            PLGameController.instance.StageComplete();
            anim.enabled = true;
            coll.gameObject.transform.position = resetPoint.position;
        }
    }



}
