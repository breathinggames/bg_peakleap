# bg_peakLeap
# Notes for Peak Flow Game
###<i>(March 1 - 3, 2017; Geneva, Switzerland; Lift Lab 2017)</i>

- created in Geneva at Lift Lab (team: Ned Birkin, Gareth Brown, Maria Frangos, Lucile Chabre, Lai-Tze Fan)


<hr />


## BACKGROUND INFO ON PEAK FLOW METER
- peak expiratory flow measures a person's maximum speed of expiration with a device called a peak flow meter
	- is able to measure level of obstruction in bronchial airways for people with asthma

- ideally, three hard breaths each time, two times a day, for at least 14 days in order to track any deviation in peak flow values
	- especially useful for doctors assessing changes in peak flow values before/after medical treatent or recommendations
	- not as useful for patients who have consistent, non-chronic peak flow values

- see these charts detailing the <a href="http://tvscn.nhs.uk/wp-content/uploads/2014/09/Paediatric-Normal-Values.pdf">normal pediatric peak flow values</a>
	- measured through factors including: height, age, sex, (and ethnicity)

## GAME DESIGN
- encourage children aged 6-12 to do a self-assessment of their peak flow by turning the peak flow meter into an element of an interactive video game
- track the changes in their peak flow with corresponding charts
- original medical advice (in Montreal) was that the user should blow regularly into the peak flow device to move a ball along a path. 
- however, in Geneva we spoke to François, a respiratory physiotherapist, about the process of blowing into the peak flow device, to get a better idea of how the device could be used as part of a game
- the point is to blow into the device as HARD as possible for a short space of time, then allow for as long as the patient needs to catch their breath. The process should be done three (and ONLY three) separate times, regardless of how 'well' it is done each time.
- therefore we changed the design from the original advice (blowing slowly and steadily into the device), as this would put the patient out of breath and produce an inaccurate peak flow measurement.
- the new design has each blow corresponding to one "turn" in the game, which is followed by a brief period of play to allow them to catch their breath, before the next "turn" begins.


## Considerations in Design
- for incentive and reward, we added stars to the course that can be collected to be used in the My Room platform
- for incentive to do the peak flow measurement properly (blowing into the device as hard/fast as possible), we created three tiers/levels so that proper blows will project the ball higher, where there are more stars to collect. 	
- doing three peak flow measurements would allow a user to project the ball higher depending on how close they have come to their maximum expected volume', with the higher platforms having more star rewards.


## To Do
- integration as a mini-game in the MyRoom hub project
- enhance the visuals and effects further
- add additional sound effects
- different obstacles courses may be added later (Lai-Tze has the designs)
- avatars to become personalized
- the level needs "desigining" - the current level is mainly for proof of concept, the floor and wall tiles can be used to easily create different layouts (remember to hold 'Ctrl' in Unity so the blocks 'snap' together
- 